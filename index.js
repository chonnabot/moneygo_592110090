/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';

import LoginPage from './src/page/LoginPage';
import HomePage from './src/page/HomePage';

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => HomePage);
