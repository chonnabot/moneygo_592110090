import React from 'react'

import Header from '../styled_components/header'
import { Background, SubmitButton, BodyContainer } from '../styled_components/styled_components'

class HomePage extends React.Component {
    render() {
        return (
            <Background>

                <Header />

                <BodyContainer>
                    <SubmitButton type="primary" >Submit</SubmitButton>

                </BodyContainer>
            </Background>


        )
    }
}

export default HomePage