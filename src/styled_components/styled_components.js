import styledCom from 'styled-components'
import { Button, InputItem, Icon } from '@ant-design/react-native'

export const Center = styledCom.View`
    align-items: center;
    justify-content: center;
`
export const Background = styledCom.View`
    backgroundColor: #778899;
    flex : 1 ;
`
export const BodyContainer = styledCom.View`
    backgroundColor: #778899;
    align-items: center;
    justify-content: center;
    flex : 1 ;
`
export const HeaderContainer = styledCom.View`
    flex-direction: row;
    align-items: center;
    justify-content: center;
    backgroundColor: #f2f2f2
    height: 50
    padding-top: 5;
    padding-bottom: 5;
`

export const HeaderText = styledCom.Text`
    font-family: verdana;
    font-size: 20px;
`
export const SubmitButton = styledCom(Button)`
    width: 40%;
    
`